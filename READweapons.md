# READ Weapons & Moves #

### Starter Weapons & Moves ###
Wooden Sword
* STR: 1,	DEF: 3,		SPEED: 3
* Moves: SLASH: ATK 3,	POKE: ATK 2,	BLOCK: DEF 2

Wooden Shield
* STR: 2,	DEF: 5,		SPEED: 4
* Moves: BLOCK: DEF 2,	PARRY: ATK 1,	DEFLECT: DEF 2

Wooden Bow
* STR: 3,	DEF: 1,		SPEED: 2
* Moves: SHOOT: ATK 3,	PIERCE: ATK 4,	SPLIT: ATK 2 X2 (Attack twice)

Wooden Spear
* STR: 2,	DEF: 2,		SPEED: 2
* Moves: THRUST: ATK 4,	PIERCE: ATK 4,	WHACK: ATK 2 X2 (Attack twice)


### Beginner Weapons & Moves ###
Iron Sword
* STR: 4,	DEF: 6,		SPEED: 4
* Moves: SLASH: ATK 3,	STRIKE: ATK 5,	BLOCK: DEF 2

Iron Shield
* STR: 5,	DEF: 8,		SPEED: 5
* Moves: BLOCK: DEF 2,	PARRY: ATK 1,	DEFLECT: DEF 2

Recurve Bow
* STR: 6,	DEF: 2,		SPEED: 3
* Moves: SHOOT: ATK 3,	PIERCE: ATK 4,	SPLIT: ATK 2 X2

Iron Spear
* STR: 5,	DEF: 4,		SPEED: 3
* Moves: THRUST: ATK 4, PIERCE: ATK 4,	WHACK: ATK 2 X2


### Mid-Level Weapons & Moves ###
Steel Sword
* STR: 7,	DEF: 9,		SPEED: 5
* Moves: SLASH: ATK 3,	STRIKE: ATK 5,	BLOCK: DEF 2

Steel Shield
* STR: 8,	DEF: 12,	SPEED: 6
* Moves: BLOCK: DEF 2,	PARRY: ATK 1,	DEFLECT: DEF 2

Compound Bow
* STR: 9,	DEF: 4,		SPEED: 4
* Moves: SHOOT: ATK 3,	PIERCE: ATK 4,	SPLIT: ATK 2 X2

Steel Spear
* STR: 8,	DEF: 4,		SPEED: 4
* Moves: THRUST: ATK 4,	PIERCE: ATK 4,	WHACK: ATK 2 X2


### Master Weapons & Moves ###
#DRAGON SWORD#
* STR: 14,	DEF: 18,	SPEED: 5
* Moves: SLASH: ATK 3,	STRIKE: ATK 5,	HELMSPLITTER: ATK 8

#AEGIS SHIELD#
* STR: 16,	DEF: 24,	SPEED: 6
* Moves: PARRY: ATK 1,	DEFLECT: DEF 2,	MORTAL_COIL: ATK 2 X4

#STAR CALLER#
* STR: 20,	DEF: 10,	SPEED: 4
* Moves: SHOOT: ATK 4,	PIERCE: ATK 4,	SKY_FALL: ATK 2 X0 (hits all enemies)

#GUNGNIR#
* STR: 18,	DEF: 10,	SPEED: 4
* Moves: THURST: ATK 4,	PIERCE: ATK 4,	SWEEP: ATK 2 X0 (hits all enemies)