# READ Enemies #


### Forest Enemies ###
Rabbits
* Rarity: Abundance,	STR: 2,		DEF: 3,		INT: Low,	SPEED: 5,	HP: 10
* Rabbits are all over the forest and are found independently from their families.

Wolves
* Rarity: Common,		STR: 5,		DEF: 5,		INT: Low,	SPEED: 7,	HP: 20
* Wolves are in large numbers because of the abundance of rabbits and come in groups of 2-4.

Foxes
* Rarity: Common,		STR: 7,		DEF: 3,		INT: Low,	SPEED: 10,	HP: 15
* Foxes and Wolves are at war with each other for rabbits and hovels.


### Sea Enemies ###
Crustaceans (Crabs,Lobsters,Shrimps)
* Rarity: Abundance,	STR: 2,		DEF: 5,		INT: Low,	SPEED: 7,	HP: 10
* Crustaceans are the easiest and most abundant source of food in Barge Water.

Fish (Bass,Tuna,Mackerel)
* Rarity: Abundance,	STR: 2,		DEF: 3,		INT: Med,	SPEED: 10,	HP: 10
* Fish are mostly caught through fishing but some adventurers enjoy the rough tousle of jumping into the sea.

Chondrichthyes (Sharks,Sting rays,Manta ray)
* Rarity: Common,		STR: 10,	DEF: 12,	INT: Med,	SPEED: 20,	HP: 30
* Chondrichthyes are noticeably deadly creatures in the sea, but are a great source of nutrients.


### Cave Enemies ###
Chelicerata (Spiders,Scorpions,Mites/Fleas)
* Rarity: Abundance,	STR: 3,		DEF: 3,		INT: Med,	SPEED: 10,	HP: 15
* Chelicerata are the most common at cave entrances where it is easier to find prey. Some may even spill out of the cave entrance.

Myriapoda (Milli- & Centi- pedes, Symphyla & Paurapoda)
* Rarity: Common(*)		STR: 10,	DEF: 20,	INT: Med,	SPEED: 20,	HP: 30
* These creatures are more deadly the bigger they get as well as getting more rare.

Unknown (Demons,Crystals,Hordes)
* Not much is known about these enemies (coming in an update) but adventurers can accidentally meet them if the venture too far.


### North-East Pass ###
Bandit/Scavenger Thugs (Weak Muscle)
* Rarity: Abundance,	STR: 10,	DEF: 10,	INT: Med,	SPEED: 15,	HP: 15
* Thugs mostly do the grunt work and as such they are always in large groups.

Bandit/Scavenger Captain (Leader)
* Rarity: Scarce,		STR: 20,	DEF: 20,	INT: High,	SPEED: 15,	HP: 30
* A captain is given special command over his thugs and as such can give buffs and is smarter in fighting tactics.

Bandit/Scavenger Archer (Ranger)
* Rarity: Common,		STR: 10,	DEF: 5,		INT: Med,	SPEED: 15,	HP: 10
* Archers' aren't cared for much but they are seen as a good scout when it comes to fighting a small group of adventurers.