# Phases of Construction #

### Starting Phase ###
#UI Running#
* Have a terminal similar to Runescape UI
* Top 'screen' is terminal output
* Bottom-Left 'screen' for player input
* Bottom-Centre 'screen' for player inventory
* Bottom-Right 'screen' for player stats

#Barge Water Functioning#
* Barge Water is layed out and is traversible
* All locations have a description and functioning interactability
* Forest, Mountains & Sea have functioning 'rooms' to encounter enemies
* North-East Pass functioning 'battles' to protect merchants

#Functioning Enemies#
* Enemy stats & moves
* Enemy instances in correct areas

#Functioning Player#
* Player stats & moves
* Player inventory & equipment
* Player interaction & movement around Barge Water

#Functioning Battles#
* Player and environment interaction
* Player and enemy battles
* HP, DEF, ATK, Moves functionality
* Defeat & Victory of battles
* Loss in Defeat, Gain & drops in Victory

#Functioning Shops#
* Merchant and Adventurers guilds stats and costs
* Guild Grading System (Player Levelling System)
* Guild Quest Board cycle