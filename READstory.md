# READ Story #

### What is Barge Water? ###

* Barge Water is the City/Town that exists next to the sea
* It harbors a population of under 1,000 most of which are villagers
* An Adventurers' guild and Merchant guild havve been set up in Barge Water
* The main food supply comes from shallow fishing and easy plantation vegetables


### What surrounds Barge Water? ###

* Sea to the east, Mountains to the North to West, Forest covering West to South to East
* The only route that merchants and supplies come in is from a path in the North-East
* There are caves around the mountain range, Dens throught the forest, and underwater hills in the deep sea


### Where do Enemies come from? ###

* There are animal based enemies in the forest (Bovines/Felines/Canines)
* There are fish in the shallow sea & crustaceans, and larger Chondrichthyes in the deeper sea
* There are insect enemies from the caves (Chelicerata/Myripoda)
* There are Bandit/Scavengers enemies on the North-East pass.


### What to do with the dropped items? ###

* Materials are sold at the Merchant guild
* Monster parts are gathered in the Adventurers guild
* Monster parts can be used to improve equipment for a cost
* Monster parts can also be sold to the Merchant guild


### Purpose of Adventurers guild ###

* To find adventurers who will become the next Hero's who will save the world
* A classing system is used to ascertain adventurer's skill